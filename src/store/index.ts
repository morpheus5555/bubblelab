/*import Vue from 'vue';
import Vuex from 'vuex';
// import ToDoModel from './models/ToDoModel';

Vue.use(Vuex);

const user = {
  state: {
    birth_date: "1985-06-15",
    birth_time: "00:00",
    birth_location: null,
    sex: "f",
    knowing_birth_time: true,
  },

  mutations: {
    addToDo(state) {
      // state.todos.push(todoModel);
    }
  },
  actions: {
    set(context, user) {

      // context.commit('addToDo', todoModel);
    }
  }
} 

export default new Vuex.Store({
  modules: {
    user
  }
});*/

import Vue from 'vue'
import Vuex from 'vuex'
import { GetterTree, MutationTree, ActionTree } from "vuex"
// import MySubModule from '@/store/submodule'

Vue.use(Vuex)

class State {
    userId: string | null = null;
}

const getters = <GetterTree<State, any>>{
};

const mutations = <MutationTree<State>>{
    setUserId(state, payload) {
        state.userId = payload;
    }
};

const actions = <ActionTree<State, any>>{
    fetchUserId(store) {
    }
};

export default new Vuex.Store({
    state: new State(),
    mutations: mutations,
    actions: actions,
    modules: {
      //   subModuleName: MySubModule,
        //other submodules
    }
})
