import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'Home',
		component: () => import('@/views/Home.vue')
	},
	{
		path: '/testonly',
		name: 'TestOnly',
		component: () => import('@/views/TestOnly.vue')
	},
	{
		path: '/loading',
		name: 'Loading',
		component: () => import('@/views/Loading.vue')
	},
	{
		path: '/tabs',
		name: 'Tabs',
		redirect: '/tabs/tab1'
	},
	{
		path: '/tabs/tab1',
		name: 'Tab1',
		component: () => import('@/views/Tab1/Tab1.vue'),
		beforeEnter: (to, from, next) => {
			if (!localStorage.me) 	next('/')
			else 					next()
		}
	},
	{
		path: '/tabs/tab1/details',
		name: 'BubbleDetails',
		component: () => import('@/views/Tab1/Modal.vue'),
		meta: { fullscreen: true },
		beforeEnter: (to, from, next) => {
			if (!localStorage.me) 	next('/')
			else if (!localStorage.bubbleDetails) 	next('/tabs/tab1')
			else 					next()
		}
	},
	{
		path: '/tabs/howto',
		name: 'HowTo',
		component: () => import('@/views/HowTo.vue')
	},
	{
		path: '/tabs/tab2',
		name: 'Tab2',
		component: () => import('@/views/Tab2.vue')
	},
	{
		path: '/tabs/tab3',
		name: 'Tab3',
		component: () => import('@/views/Tab3.vue')
	}
]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes
})

export default router
