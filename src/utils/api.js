/* eslint-disable */
export default {
  async fetchAPI(route, data) {
    const response = await fetch('https://cors-anywhere.herokuapp.com/'+`http://mybubblelab.com:8080/api${route}`, {
      method: 'POST',
      // headers: {'Authorization': 'Basic ' + btoa('apikey:demokey')},
      body: JSON.stringify(data)
    })
    
    if (!response.ok) {
      console.error('Unable to fetch bubblelab API.', response)
      return null
    }
    let txt = await response.text()

    // Currrently the (awful) API sends html AND json in the exact same response
    // This is why we need to trim the non-json content.
    let purify = (str) => {
      let first = str.indexOf('{')
      let last = str.lastIndexOf('}')
      return str.slice(first, last+1)
    }

    const json = JSON.parse(purify(txt)) 
    console.log('json', json)
    return json
  },
  async getMe () {
    const bubbles = await this.fetchAPI('/me2.php', Object.assign(JSON.parse(localStorage.user), { id: 1, apikey: 'demokey' }))
    console.log(bubbles)
    return bubbles
  },
}