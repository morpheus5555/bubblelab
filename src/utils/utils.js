
/* UTILITIES
---------------------------------------------------------------------------------------------------- */

export const sample = arr => arr[Math.floor(Math.random() * arr.length)]


export const random = (min, max) => Math.random() * (max - min) + min


export const sum = values => values.reduce((currSum, value) => currSum + value, 0)


export const mean = values => sum(values) / values.length


export const clamp = (val, min = 0, max = 1) => Math.max(min, Math.min(max, val))


export const roundTo = (number, places = 0) => Math.round(number * 10 ** places) / 10 ** places


export const lerp = (start, finish, progress) => ((finish - start) * progress) + start


export const radians = angle => angle * (Math.PI / 180)


export const degrees = angle => angle * (180 / Math.PI)


export const sigmoid = x => x / (1 + Math.abs(x))


export const BB = $el => $el.getBoundingClientRect()

export const CS = (el, property) => {
    const style = window.getComputedStyle(el)

    return (property) ? style.getPropertyValue(property) : style
}



