import Vector2D from './Vector2D'




/* CIRCLE
----------------------------------------------------------------------------------------------------*/

export default class Circle {

    constructor(id, radius, x = 0, y = 0, isPinned = false) {
        this.id = id

        this.isPinned = isPinned
        this.target = new Vector2D(0, 0)
        this.position = new Vector2D(x, y)
        this.prevPosition = new Vector2D(x, y)

        this.positionWithOffset = new Vector2D(0, 0)
        this.prevPositionWithOffset = new Vector2D(0, 0)

        this.setRadius(radius, true)
    }




    /* Actions
    --------------------------------------------------------- */

    setPosition(pos) {
        this.prevPosition = this.position
        this.position = pos.copy()
    }


    getPositionWithRadiusOffset() {
        this.prevPositionWithOffset.x = this.positionWithOffset.x
        this.prevPositionWithOffset.y = this.positionWithOffset.y

        this.positionWithOffset.x = (this.position.x - this.radius)
        this.positionWithOffset.y = (this.position.y - this.radius)

        return this.positionWithOffset
    }


    containsPoint(p) {
        const distance2 = this.position.distance2(p)

        return distance2 < this.radius2
    }


    distance2FromTarget() {
        const distance2 = this.position.distance2(this.target)

        return distance2 < this.radius2
    }


    intersects(circle) {
        const distance2 = this.position.distance2(circle.position)

        return (distance2 < this.radius2 || distance2 < circle.radius2)
    }


    setRadius(radius, setBase) {
        this.radius = radius
        this.radius2 = radius * radius

        if (setBase) {
            this.baseRadius = radius
        }
    }

}



