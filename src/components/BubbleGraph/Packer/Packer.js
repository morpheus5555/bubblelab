import Vector2D from './Vector2D'
import Circle from './Circle'

import { isCircleValid } from './_utils'




/* PACKER
----------------------------------------------------------------------------------------------------*/

export default class Packer {

    constructor({ paused = true, spacing = 0, onMove }) {
        this.circles = []
        this.target = new Vector2D(0, 0)
        this.bounds = { left: 0, top: 0, right: 0, bottom: 0 }
        this.spacing = spacing

        this.damping = 0.025
        this.centeringPasses = 1
        this.collisionPasses = 3

        this.isRunning = !paused

        this.onMove = onMove

        if (this.isRunning) {
            this.update()
        }
    }




    /* Settings
    --------------------------------------------------------- */

    setBounds({ width, height }) {
        this.bounds.right = this.bounds.left + width
        this.bounds.bottom = this.bounds.top + height
    }


    setSpacing(p) {
        this.spacing = p
    }


    setPinnedCircle(id) {
        this.circles.forEach((c) => {
            c.isPinned = c.id === id
        })
    }


    setCirclePosition(id, x, y) {
        const circle = this.circles.find(c => c.id === id)

        circle.position.x = x
        circle.position.y = y
    }


    setTarget({ x = 0, y = 0 }) {
        this.target = new Vector2D(x, y)
    }


    addCircle(circle) {
        this.addCircles([circle])
    }


    removeCircle(id) {
        this.circles = this.circles.filter(c => c.id !== id)

        if (!this.isRunning) {
            this.update()
        }
    }


    addCircles(circles) {
        if (!Array.isArray(circles) || !circles.length) return

        const newCircles = circles.filter(isCircleValid)

        if (!newCircles.length) return

        newCircles.forEach((c) => {
            let circle = c
            if (!(circle instanceof Circle)) {
                circle = new Circle(
                    circle.id,
                    circle.radius,
                    circle.position.x,
                    circle.position.y,
                    circle.isPinned,
                )
            }

            this.circles.push(circle)
            circle.target = this.target.copy()
        })

        if (!this.isRunning) {
            this.update()
        }
    }


    setCircleRadius(id, radius) {
        const circle = this.circles.find(c => c.id === id)

        if (circle) {
            circle.setRadius(radius)
        }
    }




    /* Engine
    --------------------------------------------------------- */

    applyGravity() {
        const { x, y } = this.target
        const v = new Vector2D(0, 0)

        const circleList = this.circles

        const len = circleList.length

        for (let n = 0; n < this.centeringPasses; n++) {
            for (let i = 0; i < len; i++) {
                const c = circleList[i]

                if (c.isPinned) continue

                v.x = c.position.x - x
                v.y = c.position.y - y
                v.multiply(this.damping)

                c.position.x -= v.x
                c.position.y -= v.y
            }
        }
    }


    detectCollisions() {
        const v = new Vector2D(0, 0)

        const circleList = this.circles
        const len = circleList.length


        for (let n = 0; n < this.collisionPasses; n++) {
            for (let i = 0; i < len; i++) {
                const ci = circleList[i]

                for (let j = i + 1; j < len; j++) {
                    const cj = circleList[j]
                    if (ci === cj) continue

                    const dx = cj.position.x - ci.position.x
                    const dy = cj.position.y - ci.position.y
                    const r = (ci.radius + cj.radius) + this.spacing

                    const d = ci.position.distance2(cj.position)

                    if (d < (r * r) - 0.02) {
                        v.x = dx
                        v.y = dy
                        v.normalize()

                        const inverseForce = (r - Math.sqrt(d)) * 0.5
                        v.multiply(inverseForce)

                        if (!cj.isPinned) {
                            if (ci.isPinned) v.multiply(2)

                            cj.position.x += v.x
                            cj.position.y += v.y
                        }

                        if (!ci.isPinned) {
                            if (cj.isPinned) v.multiply(2)

                            ci.position.x -= v.x
                            ci.position.y -= v.y
                        }
                    }
                }
            }
        }
    }


    keepCircleInside(circle) {
        const { radius, position } = circle
        const { x, y } = position
        const { top, right, bottom, left } = this.bounds

        if (x + radius >= right) {
            circle.position.x = right - radius
        } else if (x - radius < left) {
            circle.position.x = left + radius
        }

        if (y + radius > bottom) {
            circle.position.y = bottom - radius
        } else if (y - radius < top) {
            circle.position.y = top + radius
        }
    }




    /* Loop
    --------------------------------------------------------- */

    start() {
        if (this.isRunning) return

        this.isRunning = true
        this.update()
    }


    pause() {
        this.isRunning = false
    }


    updatePositions() {
        const circleList = this.circles
        const circleCount = circleList.length


        for (let i = 0; i < circleCount; ++i) {
            const circle = circleList[i]

            circle.prevPosition = circle.position.copy()
        }


        this.applyGravity()
        this.detectCollisions()


        for (let i = 0; i < circleCount; ++i) {
            const circle = circleList[i]

            this.keepCircleInside(circle)
        }
    }


    update() {
        this.updatePositions()

        const positions = this.circles.reduce((obj, circle) => {
            obj[circle.id] = {
                position: circle.position,
                prevPosition: circle.prevPosition,
                radius: circle.radius,
            }

            return obj
        }, {})


        this.onMove(positions)

        if (this.isRunning) {
            requestAnimationFrame(() => { this.update() })
        }
    }

}



