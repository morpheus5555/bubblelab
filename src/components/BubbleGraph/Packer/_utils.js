
/* CPACKER UTILITIES
---------------------------------------------------------------------------------------------------- */

export const isCircleValid = circle => (
    circle
    && circle.id
    && circle.radius
    && circle.position
    && typeof circle.position.x === 'number'
    && typeof circle.position.y === 'number'
)

export const isBoundsValid = bounds => (
    bounds
    && typeof bounds.width === 'number'
    && typeof bounds.height === 'number'
)



