
/* VECTOR 2D
----------------------------------------------------------------------------------------------------*/

export default class Vector2D {

    constructor(x, y) {
        this.x = x
        this.y = y
    }




    /* Actions
    --------------------------------------------------------- */

    copy() {
        return new Vector2D(this.x, this.y)
    }


    add(v) {
        this.x += v.x
        this.y += v.y

        return this
    }


    sub(v) {
        this.x -= v.x
        this.y -= v.y

        return this
    }


    multiply(factor) {
        this.x *= factor
        this.y *= factor

        return this
    }


    rotate(angle) {
        const { x, y } = this

        this.x = x * Math.cos(angle) - Math.sin(angle) * y
        this.y = x * Math.sin(angle) + Math.cos(angle) * y

        return this
    }


    setAngle(angle) {
        const l = this.length()

        this.x = Math.cos(angle) * l
        this.y = Math.sin(angle) * l

        return this
    }


    setLength(length) {
        const l = this.length()

        if (l) {
            this.mul(length / l)
        } else {
            this.x = this.y = length
        }

        return this
    }


    normalize() {
        const l = this.length()

        this.x /= l
        this.y /= l

        return this
    }


    angle() {
        return Math.atan2(this.y, this.x)
    }


    collidesWith(rect) {
        return this.x > rect.x
            && this.y > rect.y
            && this.x < rect.x + rect.width
            && this.y < rect.y + rect.height
    }


    length() {
        const length = Math.sqrt((this.x * this.x) + (this.y * this.y))

        return (length < 0.005 && length > -0.005) ? 0.000001 : length
    }


    // Squared length
    length2() {
        const length2 = (this.x * this.x) + (this.y * this.y)

        return (length2 < 0.005 && length2 > -0.005) ? 0 : length2
    }


    distance(v) {
        return Math.sqrt(this.distance2(v))
    }


    // Squared distance
    distance2(v) {
        const deltaX = this.x - v.x
        const deltaY = this.y - v.y

        return (deltaX * deltaX) + (deltaY * deltaY)
    }


    equals(test) {
        return typeof test === 'object' && this.x === test.x && this.y === test.y
    }

}



